# -*- coding: utf-8 -*-
"""
Created on Sat Mar 20 13:18:12 2021

@author: Andrew
"""
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import data_science_functions as dfunc


        
        
datapath = "C:\\Users\\Andrew\\Documents\\DSets\\weatherAUS.csv"

# Takes the date field and processes it into a set of 4 variables 
aus_rain=pd.read_csv(datapath,parse_dates=["Date"])
aus_rain["weekday"] = aus_rain["Date"].dt.weekday
aus_rain["month"] = aus_rain["Date"].dt.month
aus_rain["month_day"] = aus_rain["Date"].dt.day
aus_rain["year"] = aus_rain["Date"].dt.year

#plotting correlation heat map to look for any strong correlations
dfunc.correlation_heatmap_plot(aus_rain,True,[16,12])
plt.show()

#Grouping entries by month and location to see where gaps in the data may be
entries_per_month =  aus_rain.groupby(["month","Location"])["RainToday"].count().to_frame()

#Counts location and monthwise for days where it rains
month_rain = aus_rain[aus_rain["RainToday"]=="Yes"
                      ].groupby(["month","Location"
                      ])["RainToday"].count().reset_index(name="RainToday")
                                 
                                 
#~~~~~~~~~~
#Cleaning up data 
#~~~~~~~~~~
month_rain = pd.DataFrame(month_rain)
 #renames a column 
month_rain = month_rain.rename(columns={"month":"Month"})
# Creating an average for rain count by month and location
month_rain["RainTodayAvg"] = month_rain["RainToday"].div(10)

#sorting the grouped data by raid today counts
month_rain = month_rain.sort_values(by = "RainToday",ascending =False)

#~~~~~~~~~~
#Extracts the top 20 months with the most rain per locaiton
#~~~~~~~~~~
#data selection, top 20 
plotting_sample = month_rain.iloc[0:20]

# figure creation
fig1, ax1 =plt.subplots(figsize=(5,4))

#Bar plotting with seaborn
p = sns.barplot(data = plotting_sample, x="Month",y="RainTodayAvg",hue="Location"
               ,palette="muted",ax=ax1)
plt.title("Highest average days of rain sorted by month and location")

#Moves the legends outside of the plot area
plt.legend(bbox_to_anchor=(1.01, 1),borderaxespad=0)

plt.show()


#~~~~~~~~~~
#Extracts the bottom 20 months with the most rain per locaiton
#~~~~~~~~~~
#Selection of the 20 lowest values 
plotting_sample_low = month_rain.tail(20)

# figure creation
fig2, ax2 =plt.subplots(figsize=(5,4))


#Bar plotting with seaborn
p = sns.barplot(data = plotting_sample_low, x="Month",y="RainTodayAvg",hue="Location"
               ,palette="muted",ax=ax2)
plt.title("Lowest average days of rain sorted by month and location")

#Moves the legends outside of the plot area
plt.legend(bbox_to_anchor=(1.01, 1),borderaxespad=0)
plt.show()


