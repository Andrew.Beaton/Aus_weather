# -*- coding: utf-8 -*-
"""
Created on Fri Mar 19 14:27:44 2021

@author: Andrew
"""
from sklearn.model_selection import train_test_split
import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn import svm
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import time 
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

def extract_numeric(data):
    #This function returns the columsn that are numeric in nature
    numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
    numerical_cols = [cname for cname in data.columns if 
                data[cname].dtype in numerics]
    return numerical_cols
    
    
def extract_cat_small(data,cutp):
     #Returns a list of catagorical columns which have fewer than 10 entry types
    cat_cols = [cname for cname in data.columns if
                    data[cname].nunique() < cutp and 
                    data[cname].dtype == "object" ]
    return cat_cols

def  check_na(data):
    #Returns a list of input coloumns that are less than 30% n/a
    cols = [cname for cname in data.columns
        if data[cname].isna().sum()/data[cname].count() <0.3 ]  
    return cols        
        
def na_percent(data):
    # This function takes in data and returns the percentage level of NA
    na_cols = dict()
    for cname in data.columns:
        #checks na percentage level
        naPer = data[cname].isna().sum()/data[cname].count()
        if naPer >0:
            na_cols[cname] = round(naPer,3)
    return na_cols

def modelpipeline(model):
    var_pipe = Pipeline(
        steps = [("preprocessor",preprocessor),
                 ("TestedModel",model)
                 ])
    return var_pipe


datapath = "C:\\Users\\Andrew\\Documents\\DSets\\weatherAUS.csv"

aus_rain=pd.read_csv(datapath)

#Drop any rows where target is Na 
aus_rain.dropna(subset=['RainTomorrow'],inplace=True,axis = 0)

#Extract feature to be predicted 
Y = aus_rain.RainTomorrow


#Remove the target data  and rename to X 
aus_rain.drop(["RainTomorrow"],axis=1, inplace =True)
X = aus_rain

#Splitting data for use in models 
train_X_full, val_X_full, train_y_full, val_y_full = train_test_split(X, Y,train_size=0.8, test_size=0.2, random_state = 0)

#Calls functions to split the data into numerical and catagorical
numerical_cols = extract_numeric(train_X_full)
cat_cols = extract_cat_small(train_X_full,17)

#Checks if the  columns can be used imputer
numerical_cols_final = check_na(train_X_full[numerical_cols])
cat_cols_final = check_na(train_X_full[cat_cols])

#Pre encoding
enc = preprocessing.LabelEncoder()
enc.fit(val_y_full)
val_y_full = enc.transform(val_y_full)

enc.fit(train_y_full)
train_y_full = enc.transform(train_y_full)

# define one hot encoding
encoder = OneHotEncoder(sparse=False)

# transform data
#train_y_full = encoder.fit_transform(train_y_full)
#val_y_full = encoder.fit_transform(val_y_full)

#Transposing data before use in model
train_y_full=train_y_full.T
val_y_full=val_y_full.T

#Pipeline for Catagorical Data
categorical_transformer =  Pipeline(steps  = [
        ("imputer", SimpleImputer(strategy = "most_frequent")),
        ("onehot" , OneHotEncoder(handle_unknown ="ignore"))
        ])


# Pipeline for Numerical data
numerical_transformer = SimpleImputer(strategy = "constant")


#Combine pre processing
preprocessor = ColumnTransformer(
        transformers  = [
                ("num",  numerical_transformer, numerical_cols_final),
                ("cat", categorical_transformer, cat_cols_final),
                ])#

#Define the model to use 

Rforestmodel = LogisticRegression( random_state=0)




#Execution pipeline
prep_execute = Pipeline(
    steps = [("preprocssing",preprocessor),
             ("boost_model",Rforestmodel)]
    )


#~~~~~~~~~~
#logistic Regression
#~~~~~~~~~~
t0=time.time()
regpipe = modelpipeline(Rforestmodel)
regpipe.fit(train_X_full,train_y_full)
predictions = regpipe.predict(val_X_full)

print("Accuracy from logistic Regression :")
print(accuracy_score(val_y_full, predictions))
print('Time taken :' , time.time()-t0)


#~~~~~~~~~~
#SVM 
#~~~~~~~~~~
t0=time.time()
svm_linear = svm.LinearSVC(random_state=0, tol=1e-5)
#Execution pipeline
svmpipe = modelpipeline(svm_linear)
svmpipe.fit(train_X_full,train_y_full)
predictions_svmlinear = svmpipe.predict(val_X_full)

print("Accuracy from SVM linear :")
print(accuracy_score(val_y_full, predictions_svmlinear))
print('Time taken :' , time.time()-t0)

#~~~~~~~~~~
#Random Forest Classifier 
#~~~~~~~~~~
t0=time.time()
#Random forest could be optimised with chaning n estimators 
RFC = RandomForestClassifier(n_estimators = 20 , n_jobs = 6 )
#Execution pipeline
RFCpipe = modelpipeline(RFC)
RFCpipe.fit(train_X_full,train_y_full)
predictions_RFCpipe = RFCpipe.predict(val_X_full)

print("Accuracy from Random forest classifier linear :")
print(accuracy_score(val_y_full, predictions_RFCpipe))
print('Time taken :' , time.time()-t0)



#~~~~~~~~~~
#Stocastic Gradient Descent
#~~~~~~~~~~
# Timing the model 
t0=time.time()


#SGD is sensitive to feauture scalling so normalise values


scaler = StandardScaler()

#Creates copies of the data so as to not poison other examples
train_X_full_SGD = train_X_full
val_X_full_SGD = val_X_full 

# Note this could be placed in a pipeline for production work
scaler.fit(train_X_full_SGD[numerical_cols_final])  # fit only on training data
X_train = scaler.transform(train_X_full_SGD[numerical_cols_final])
X_test = scaler.transform(val_X_full_SGD[numerical_cols_final])  # apply same transformation to test data


#Execution pipeline
SGD = SGDClassifier(loss="hinge", penalty="l2", max_iter=5e4)
prep_execute.fit(train_X_full_SGD,train_y_full)
predictions_prep_execute = prep_execute.predict(val_X_full_SGD)

print("Accuracy from stocastic gradietn descent :")
print(accuracy_score(val_y_full, predictions_prep_execute))
print('Time taken :' , time.time()-t0)


cm = confusion_matrix(val_y_full, predictions_RFCpipe, labels=RFCpipe.classes_)
disp = ConfusionMatrixDisplay(confusion_matrix=cm,
                              display_labels=RFCpipe.classes_)
disp.plot()