# -*- coding: utf-8 -*-
"""

@author: Andrew
"""


import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def correlation_heatmap_plot(data,save_var,size):
    data_cor = data.corr()
    # Draw a heatmap with the numeric values in each cell
    f, ax = plt.subplots(figsize=size)
    sns.heatmap(data_cor, square=True, annot=True, fmt='.2f', linecolor='white')

    ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
    ax.set_yticklabels(ax.get_yticklabels(), rotation=30)  
    
    if save_var == True :
        plt.savefig("Correlation Heatmap",dpi = 300)
        
def extract_numeric(data):
    #This function returns the columsn that are numeric in nature
    numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
    numerical_cols = [cname for cname in data.columns if 
                data[cname].dtype in numerics]
    return numerical_cols
    
    
def extract_cat_small(data,cutp):
     #Returns a list of catagorical columns which have fewer than 10 entry types
    cat_cols = [cname for cname in data.columns if
                    data[cname].nunique() < cutp and 
                    data[cname].dtype == "object" ]
    return cat_cols

def  check_na(data):
    #Returns a list of input coloumns that are less than 30% n/a
    cols = [cname for cname in data.columns
        if data[cname].isna().sum()/data[cname].count() <0.3 ] 
    return cols        
        
def na_percent(data):
    na_cols = dict()
    for cname in data.columns:
        naPer = data[cname].isna().sum()/data[cname].count()
        if naPer >0:
            na_cols[cname] = round(naPer,3)
    return na_cols